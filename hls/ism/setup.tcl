//  Catapult Ultra Synthesis 2021.1/950854 (Production Release) Mon Aug  2 21:36:02 PDT 2021
//  
//          Copyright (c) Siemens EDA, 1996-2021, All Rights Reserved.
//                        UNPUBLISHED, LICENSED SOFTWARE.
//             CONFIDENTIAL AND PROPRIETARY INFORMATION WHICH IS THE
//                   PROPERTY OF SIEMENS EDA OR ITS LICENSORS.
//  
//  Running on Linux msilvaol@mes 3.10.0-1160.66.1.el7.x86_64 x86_64 aol
//  
//  Package information: SIFLIBS v23.7_0.0, HLS_PKGS v23.7_0.0, 
//                       SIF_TOOLKITS v23.7_0.0, SIF_XILINX v23.7_0.0, 
//                       SIF_ALTERA v23.7_0.0, CCS_LIBS v23.7_0.0, 
//                       CDS_PPRO v10.5a, CDS_DesigChecker v2021.1, 
//                       CDS_OASYS v20.1_3.6, CDS_PSR v20.2_1.8, 
//                       DesignPad v2.78_1.0
//  
project new -name Catapult_project
solution file add ../../src/ism/ism_tb.cpp -type C++ -exclude true
solution file add ../../src/ism/ism.h -type CHEADER
solution file add ../../src/ism/ism.cpp -type C++
options set Input/CppStandard c++11
directive set -DESIGN_GOAL latency
directive set -REGISTER_THRESHOLD 400
directive set -MEM_MAP_THRESHOLD 400
directive set -ROM_THRESHOLD 400
go new
go compile
solution library add mgc_Altera-Arria-10-2_beh -- -rtlsyntool Quartus -manufacturer Altera -family {Arria 10} -speed 2 -part 10AX115R3F40E2SG
solution library add Altera_DIST
solution library add Altera_M20K
solution library add Altera_MLAB
go libraries
directive set -CLOCKS {clk {-CLOCK_PERIOD 3.57 -CLOCK_EDGE rising -CLOCK_HIGH_TIME 1.785 -CLOCK_OFFSET 0.000000 -CLOCK_UNCERTAINTY 0.0 -RESET_KIND sync -RESET_SYNC_NAME rst -RESET_SYNC_ACTIVE high -RESET_ASYNC_NAME arst_n -RESET_ASYNC_ACTIVE low -ENABLE_NAME {} -ENABLE_ACTIVE high}}
directive set /ism -START_FLAG start
go assembly
#directive set /ism/s:rsc -MAP_TO_MODULE {[DirectInput]}
directive set /ism/x:rsc -MAP_TO_MODULE ccs_ioport.ccs_in
directive set /ism/s.sel:rsc -MAP_TO_MODULE ccs_ioport.ccs_in
directive set /ism/s.ena:rsc -MAP_TO_MODULE ccs_ioport.ccs_in
directive set /ism/y:rsc -MAP_TO_MODULE ccs_ioport.ccs_out
directive set /ism/core/main -PIPELINE_INIT_INTERVAL 1
if { [info exists ::env(BUILD)] } {
	go extract
	# Running C simulation
	flow run /SCVerify/launch_make ./scverify/Verify_orig_cxx_osci.mk {} SIMTOOL=osci sim
	# Comparing C simulation to RTL output
	flow run /SCVerify/launch_make ./scverify/Verify_rtl_vhdl_msim.mk {} SIMTOOL=msim sim
}
