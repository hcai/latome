#!/bin/bash
if grep 'no errors were found' catapult.log -q
then
echo 'C simulation finished with success'
else
echo 'C simulation DID NOT finish with success'
exit 1
fi

# Checking C/RTL simulation
if grep 'Simulation PASSED' catapult.log -q
then
echo 'SCVerify C/RTL simulation finished with success'
else
echo 'SCVerify C/RTL simulation DID NOT finish with success'
exit 1
fi
