# Upgraded LATOME HLS/RTL design and verification files

Entry source files, i.e. the C/C++ units or modules originally created in RTL, are located at the `src` folder. 
Use instructions from section 1) to clone the respository. The same repository clone is used for HLS and RTL flows. 

## 1) C/C++ Integrated Development and Verification Flow 

The C/C++ verification flow uses the Eclipe IDE. The respective files are located at the `ide` folder.
Start the C/C++ simulation flow using the following instructions:

0) Open eclipse with the workpsace of preference.
1) File >> Import >> Git >> Projects from Git >> Next >> Clone URI >> Next
    - If it's not possible to connect to the repository, it will be necessary to [configure the SSH key](https://gitlab.cern.ch/help/user/ssh.md) following the steps below.
      - Generate a new SSH key: `ssh-keygen -t rsa -C mes`
      - Copy the public key: `cat .ssh/id_rsa.pub`
      - Add the key in: Gitlab >> Preferences >> SSH Keys
2) Enter the "Clone with SSH" URI of this git repository in the URI field and click Next twice
3) Select the directory where the repository should be located (this is not yet the location of the eclipse project), and click Next twice
4) Review the eclipse projects being imported and click Finish
5) Press The Run button and check if the output is as follows (except the seed that is different for each execution):

```
Started pseudo-random engine with initial random seed = 3964468372. To increase entropy, the seed is changed every test execution.
Note: To reproduce the same stimulus as the current test execution, set the initial_seed variable to 3964468372.
Checking mapping type EMBA_EMECA_1.
Starting test with 384 input values and 116 setting values for 100 BCs.
Checked 320 output values for 100 BCs.
Checking mapping type EMBA_EMECA_2.
Starting test with 384 input values and 116 setting values for 100 BCs.
Checked 320 output values for 100 BCs.
(...)
Checking mapping type FCAL2A.
Starting test with 384 input values and 116 setting values for 100 BCs.
Checked 320 output values for 100 BCs.
Checking mapping type FCAL2C.
Starting test with 384 input values and 116 setting values for 100 BCs.
Checked 320 output values for 100 BCs.
Simulation with initial seed 3964468372 finished with success for 116 mappings and 100 BCs, no errors were found :)
```

## 2) HLS Design Flow

The High Level Synthesis (HLS) design flow uses the Siemens Catapult tool. The respective files are located at the `hls` folder.

Start the HLS design flow using the following instructions:

```
cd hls/ism
catapult -f setup.tcl
```

Use `BUILD=1 catapult -f setup.tcl -shell` instead in case you want to generate the output RTL files and run C and C/RTL simulations. 
The CI engine uses the `setup.tcl` to run the HLS flow, so make the changes on this file ensure the command `BUILD=1 catapult -f setup.tcl -shell` finishes without errors. 

## 3) RTL design flow

The Register Transfer Level (RTL) design flow uses the Quartus Prime tool. The respective files are located at the `rtl` folder.

Note that this step depends on the  HLS-generated RTL files. By default the verilog output file from the first HLS solution is added to the Quartus project, e.g. `../../hls/ism/Catapult_project/ism.v1/concat_rtl.v` (file automatically generated, i.e. not in git).  

You also need to generate a design explorer wrapper in case it does not exist yet. One can automatically generate a design exploration wrapper by copying the module definition, e.g. `ism` module definition from `hls/ism/Catapult_project/ism.v1/concat_rtl.v` (file automatically generated, i.e. not in git) and pasting it at the SystemVerilog Design Explorer Generator tool available at [http://marcosoliveira.ch/edatools/](http://marcosoliveira.ch/edatools/). Then click Run, copy the result to the file, e.g. `rtl/ism/ism_de.sv`

Start the RTL design flow using the following instructions:

```
cd rtl/ism
# Generating quartus project
quartus_sh -t project.tcl
# Compiling project if desired (generates timing reports and FPGA binary files)
quartus_sh -t flow.tcl
# Opening project if desired
quartus ism_de.qpf
```

If you need to modify project settings, modify them using the Quartus GUI, and then overwrite `rtl/ism/project.tcl` file using the automatic TCL generator available in the Quartus menu Project >> Generate TCL file for Project. 
The CI engine uses the `project.tcl` and `flow.tcl` to run the RTL flow, so make the changes on this file ensure the commands above finish without errors. 
