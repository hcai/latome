#include "ism.h"

#include <mc_scverify.h>

#pragma hls_design ccore
#pragma hls_ccore_type combinational
void mux (const x_type x[M],
		const s_type s,
		x_type &y)
{
#pragma unroll
	for (int i = 0; i < M; ++i) {
		if (i == s.sel) {
			y = x[i];
		}
	}
	if (s.ena == 0) {
		y = 0;
	}
}

#pragma hls_design ccore
#pragma hls_ccore_type combinational
void mux_low_level (const x_type x[M],
		const s_type s,
		x_type &y)
{
	x_type x_int[1 << AW];
	// Reading input
#pragma unroll
	in_loop : for (int i = 0; i < 1 << AW; ++i) {
		if (i < M) {
			x_int[i] = x[i];
		}
	}
	// multiplexing
	int sel_int = s.sel;
	int sel_off;
	int index;
#pragma unroll
	b_loop : for (int b = AW-1; b >= 0; --b) {
		sel_off = sel_int & 1;
#pragma unroll
		o_loop : for (int o = 0; o < (1 << b); ++o) {
			//computing the respective stage input index for the respective stage output
			index = (o << 1) + sel_off;
			// Not all the 2-input multiplexer from the first stage are needed, so they
		    // will be optimized away. One can decide to remove them at the code or leave it
			// to the synthesis optimization engine. Results from the HLS resource estimation
			// indicate that is better to not remove them here.
			//if (index < M) {
				x_int[o] = x_int[index];
			//}
		}
		sel_int = sel_int >> 1;
	}
	y = x_int[0];
	// masking
	if (s.ena == 0) {
		// sets MSB (valid flag) to 1 and others to 0
		y = mask_value;
	}
}

#pragma hls_design top
void CCS_BLOCK(ism) (
		const x_type x[I],
		const s_type s[O],
		x_type y[O])
{
	x_type x_r[M];
	x_type y_r[O];
#pragma unroll
	mux_loop : for (int o = 0; o < O; ++o) {
#pragma unroll
		x_r_loop : for (int j = 0; j < M; ++j) {
			x_r[j] = x[ism_cgf[o][j]];
		}
		mux_low_level(x_r,s[o],y_r[o]);
		y[o] = y_r[o];
	}


}
