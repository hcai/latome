#include "ism.h"
#include <ac_fixed.h>

// Include utility headers
#include <assert.h>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <vector>
#include <math.h>
#include<bits/stdc++.h>
#include "../../src/common/rapidcsv.h"
#include <mc_scverify.h>

std::vector<int> GenerateRandomVector(int NumberCount,int minimum, int maximum, unsigned &seed) {
    std::vector<int> values(NumberCount);
    std::uniform_int_distribution<> dis(minimum, maximum);
    std::mt19937 gen{seed};
    std::generate(values.begin(), values.end(), [&](){ return dis(gen); });
    // Generating next seed
    std::uniform_int_distribution<> dis_full(0,pow(2,31)-1);
    seed = dis_full(gen);
    return values;
}


using namespace std;

CCS_MAIN (int argc, char *argv[])
{
	// Simulation parameters
	bool debug = false; // show debugging messages
	int n_bc = 100; // number of BCs to be tested
	// End simulation parameters

	//Generating random generator
    std::random_device rd;
    unsigned initial_seed = rd();
    //initial_seed = 935842130;
    unsigned seed = initial_seed;
    std::cout << "Started pseudo-random engine with initial random seed = " << initial_seed << ". To increase entropy, the seed is changed every test execution.\nNote: To reproduce the same stimulus as the current test execution, set the initial_seed variable to " << initial_seed << "." << std::endl;

	x_type x[I];
	s_type s[O];
	x_type y[O];

	rapidcsv::Document ism_s_file("../../dat/ism/ism_settings.csv", rapidcsv::LabelParams(0, 0));
	rapidcsv::Document ism_e_file("../../dat/ism/ism_enable.csv", rapidcsv::LabelParams(0, 0));
	rapidcsv::Document ism_m_file("../../dat/ism/ism_mapping.csv", rapidcsv::LabelParams(0, 0));
	std::vector<std::string> types = ism_s_file.GetRowNames();
	//std::vector<std::string> types = {"EMECA_HECA_1"};

    std::vector<int> x_int;
    std::vector<int> sel_int;
    std::vector<int> ena_int;
    std::vector<int> m_int;
    for (auto & type : types) {
    	std::cout << "Checking mapping type " << type << "." << std::endl;
    	// Reading switch matrix settings and the index of the required multiplexer outputs
		sel_int = ism_s_file.GetRow<int>(type);
		ena_int = ism_e_file.GetRow<int>(type);
		m_int = ism_m_file.GetRow<int>(type);
		for (unsigned o = 0; o < sel_int.size(); ++o) {
			s[o].sel = sel_int[o];
			s[o].ena = ena_int[o];
		}
    	std::cout << "Starting test with " << I << " input values and " << ism_s_file.GetRowCount() << " setting values for "<< n_bc << " BCs." << std::endl;
		for (int bc = 0; bc < n_bc; ++bc) {
			// Reading input stimulus
			//x_int = ism_x_file.GetRow<int>(std::to_string(bc));
			x_int = GenerateRandomVector(I, 0, pow(2,DW)-1, seed);
			for (unsigned i = 0; i < x_int.size(); ++i) {
				x[i] = x_int[i];
				if (debug) {
					std::cout << "Read BC " << bc << " x[" << i << "] = " << x[i] << "." << std::endl;
				}
			}
			// Invoking DUT
			CCS_DESIGN(ism)(x,s,y);
			// Checking output
			for (unsigned o = 0; o < m_int.size(); ++o) {
				if (m_int[o] != -1) {
					if (x[m_int[o]] != y[o]) {
						std::cout << "Error found: x[m_int[" << o << "]] = " << x[m_int[o]] << " != " << " y[" << o << "] = " << y[o] << " for mapping type " << type << "." << std::endl;
						CCS_RETURN(-1);
					}
				} else {
					if (y[o] != mask_value) {
						std::cout << "Error found: y[" << o << "] = " << y[o] << " is disabled but the value is different than 0 for mapping type " << type << "." << std::endl;
						CCS_RETURN(-1);
					}
				}
				if (debug) {
					std::cout << "Read BC " << bc << " s[" << o << "].sel = " << s[o].sel << "." << std::endl;
					std::cout << "Read BC " << bc << " m_int[" << o << "] = " << m_int[o] << ", x[m_int[" << o << "]] = " << x[m_int[o]] <<  "." << std::endl;
					std::cout << "Read BC " << bc << " y[" << o << "] = " << y[o] << "." << std::endl;
				}
			}
		}
		std::cout << "Checked " << sel_int.size() << " output values for " << n_bc << " BCs." << std::endl;
    }
	cout << "Simulation with initial seed " << initial_seed << " finished with success for " << types.size() << " mappings and " << n_bc << " BCs, no errors were found :)" << endl;
	CCS_RETURN(0);

}
