#!/bin/bash
if grep 'Quartus Prime Timing Analyzer was successful. 0 errors, 0 warnings' output_files/ism_de.sta.rpt -q
then
echo 'Static Timing Analysis finished without errors and warnings'
else
echo 'Static Timing Analysis finished with errors and/or warnings'
exit 1
fi

