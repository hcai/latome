// Automatic generated design exploration module
module ism_de (
input logic  clk ,
input logic  rst ,
input logic [4991:0] x_rsc_dat ,
output logic  x_rsc_triosy_lz ,
input logic [1279:0] s_sel_rsc_dat ,
output logic  s_sel_rsc_triosy_lz ,
input logic [319:0] s_ena_rsc_dat ,
output logic  s_ena_rsc_triosy_lz ,
output logic [4159:0] y_rsc_dat ,
output logic  y_rsc_triosy_lz ,
input logic  start_sync_vld 
);

// Registers declaration
reg  rst_reg ;
reg [4991:0] x_rsc_dat_reg ;
reg  x_rsc_triosy_lz_reg ;
reg [1279:0] s_sel_rsc_dat_reg ;
reg  s_sel_rsc_triosy_lz_reg ;
reg [319:0] s_ena_rsc_dat_reg ;
reg  s_ena_rsc_triosy_lz_reg ;
reg [4159:0] y_rsc_dat_reg ;
reg  y_rsc_triosy_lz_reg ;
reg  start_sync_vld_reg ;

// Registering interface signals for clock clk
always @ (posedge clk) begin
    rst_reg <= rst;
    x_rsc_dat_reg <= x_rsc_dat;
    x_rsc_triosy_lz <= x_rsc_triosy_lz_reg;
    s_sel_rsc_dat_reg <= s_sel_rsc_dat;
    s_sel_rsc_triosy_lz <= s_sel_rsc_triosy_lz_reg;
    s_ena_rsc_dat_reg <= s_ena_rsc_dat;
    s_ena_rsc_triosy_lz <= s_ena_rsc_triosy_lz_reg;
    y_rsc_dat <= y_rsc_dat_reg;
    y_rsc_triosy_lz <= y_rsc_triosy_lz_reg;
    start_sync_vld_reg <= start_sync_vld;
end // always @ (posedge clk)

// Instantiating design ism
ism ism_inst (
.clk(clk),
.rst(rst_reg),
.x_rsc_dat(x_rsc_dat_reg),
.x_rsc_triosy_lz(x_rsc_triosy_lz_reg),
.s_sel_rsc_dat(s_sel_rsc_dat_reg),
.s_sel_rsc_triosy_lz(s_sel_rsc_triosy_lz_reg),
.s_ena_rsc_dat(s_ena_rsc_dat_reg),
.s_ena_rsc_triosy_lz(s_ena_rsc_triosy_lz_reg),
.y_rsc_dat(y_rsc_dat_reg),
.y_rsc_triosy_lz(y_rsc_triosy_lz_reg),
.start_sync_vld(start_sync_vld_reg)
);

endmodule // ism_de